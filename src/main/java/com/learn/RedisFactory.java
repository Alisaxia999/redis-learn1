package com.learn;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.net.URI;

public class RedisFactory {
    private static JedisPool jedisPool = null;
    private RedisFactory(){

    }
    public static JedisPool getInstance(){
        if (jedisPool == null){
            synchronized (RedisFactory.class){
                if (jedisPool == null){
                    JedisPoolConfig config = new JedisPoolConfig();

                    config.setMaxTotal(8);
                    config.setMaxIdle(8);
                    config.setMinIdle(8);

                    config.setTestOnBorrow(true);
                    config.setTestOnReturn(true);
                    config.setMaxWaitMillis(3000);
                    config.setMinEvictableIdleTimeMillis(60);
                    config.setTimeBetweenEvictionRunsMillis(30);
                    config.setBlockWhenExhausted(false);
                    URI uri = URI.create("redis://192.168.198.139:6379");  // 我直接在Mac上运行，不需要远程连接因此可以直接用本地回环地址

                    jedisPool = new JedisPool(config, uri, 2000, 2000);

                }
            }
        }
        return jedisPool;

    }

}
