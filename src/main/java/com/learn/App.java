package com.learn;

import redis.clients.jedis.Jedis;

public class App {
    public static void main(String[] args) {
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            resource.set("key", "sno1");
            resource.del("key");

        }finally {
            resource.close();
        }

    }

}
